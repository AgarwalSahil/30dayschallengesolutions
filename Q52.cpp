#include <iostream>
using namespace std;

int main()
{
    int i = 1, j;
    while (i <= 5)
    {
        j = 1;
        while (j <= 11)
        {
            if (j < 6 - i)
                cout << " ";
            if (j == 6 - i || j==6+i)
                cout << "C";
            if (j == 6)
                cout << "W";
            if (j > (6 - i) && j < 6)
                cout << " ";
            if (j > (6) && j < 6 + i)
                cout << " ";
            
            j++;
        }

        cout << endl;
        i++;
    }
}