import java.util.*;

public class q7 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the string: ");
        String str = in.next();
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            for (int j = i; j < str.length(); j++) {
                StringBuilder str1 = new StringBuilder(str.substring(i, j + 1));
                String str2 = str.substring(i, j + 1);
                str1.reverse();
                if (str2.equals(str1.toString())) count++;
            }
        }
        System.out.println(count);
    }
}